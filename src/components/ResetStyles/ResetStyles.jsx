import React from 'react';
import { Style } from 'radium';

import resetRules from './resetRules';

const ResetStyles = () =>
    <Style
        radiumConfig={{ userAgent: 'all' }}
        rules={resetRules}
    />;

export default ResetStyles;
