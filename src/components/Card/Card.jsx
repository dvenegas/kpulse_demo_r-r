
import React from 'react';

const MainComponent = () => (
    <div className="card">
        <div className="card__header">
            <h1>KarmaPulse</h1>
        </div>
        <div className="card__info">
            <figure className="card__info__img">
                <img src="https://pbs.twimg.com/profile_images/509141833864065024/C9luH5QO.jpeg" alt="" />
            </figure>
            <div className="card__info__text">
                <h1>Daniel Venegas</h1>
                <h2>front end developer</h2>
            </div>
        </div>
    </div>
);

export default MainComponent;
