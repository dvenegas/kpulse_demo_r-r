/**
 * Header styles
 */
export default (scopeSelector, theme) => {
    const {
        card,
    } = theme.myApp;

    return {
        [`${scopeSelector} .card`]: {
            width: card.width,
            height: 'auto',
            boxSizing: 'border-box',
            padding: card.padding,
            borderRadius: card.borderRadius,
            margin: card.margin,
            background: card.background,
            border: card.border,
            boxShadow: card.boxShadow,
            overflow: 'hidden',
            transitionProperty: 'all',
            transitionDuration: '.4s',
        },
        [`${scopeSelector} .card__header`]: {
            width: card.header.width,
            height: 'auto',
            boxSizing: 'border-box',
            textAlign: 'center',
            padding: card.header.padding,
            background: card.header.background,
            border: card.header.border,
            fontWeight: card.header.fontWeight,
            textShadow: card.header.textShadow,
            boxShadow: card.boxShadow,
            color: card.header.color,
            transitionProperty: 'all',
            transitionDuration: '.4s',
        },
        [`${scopeSelector} .card__info`]: {
            width: card.info.width,
            height: 'auto',
            display: 'flex',
            justifyContent: 'flex-start',
            alignItems: 'center',
            boxSizing: 'border-box',
            padding: card.info.padding,
            color: card.info.color,
            transitionProperty: 'all',
            transitionDuration: '.4s',
        },
        [`${scopeSelector} .card__info__img`]: {
            width: card.info.img.width,
            height: card.info.img.height,
            margin: card.info.img.margin,
            border: card.info.img.border,
            borderRadius: card.info.img.borderRadius,
            overflow: 'hidden',
        },
        [`${scopeSelector} .card__info__img img`]: {
            maxWidth: '100%',
            height: 'auto',
        },
    };
};
