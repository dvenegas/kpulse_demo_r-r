import React, { PropTypes } from 'react';
import { Style } from 'radium';

const propTypes = {
    scopeSelector: PropTypes.string.isRequired
};

const ImplementationStyles = ({ scopeSelector }) => (
    <Style
        scopeSelector={scopeSelector}
        radiumConfig={{
            userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36'
        }}
        rules={{
            '': {
                flex: '1',
                height: '100vh',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                overflowY: 'scroll',
                background: 'linear-gradient(45deg, hsla(264, 98%, 46%, 1) 0%, hsla(264, 98%, 46%, 0) 70%),'+
            		            'linear-gradient(135deg, hsla(14, 99%, 45%, 1) 10%, hsla(14, 99%, 45%, 0) 80%),'+
            		            'linear-gradient(225deg, hsla(191, 93%, 42%, 1) 10%, hsla(191, 93%, 42%, 0) 80%),'+
            		            'linear-gradient(315deg, hsla(187, 97%, 47%, 1) 100%, hsla(187, 97%, 47%, 0) 70%);'
            }
        }}
    />
);

ImplementationStyles.propTypes = propTypes;

export default ImplementationStyles;
