
import React, { PropTypes } from 'react';

const propTypes = {
    handler: PropTypes.func.isRequired
};

const Header = ({ handler }) => (
    <div className="header">
        <div className="header__title">
            <img src="http://www.karmapulse.com/assets/img/logo.svg" alt="Logo" />
            <h1> Demo | Radium</h1>
        </div>
        <div className="header__themes">
            <div
                className="header__themes__item theme1"
                onClick={() => handler('theme1')}
            >
            </div>
            <div
                className="header__themes__item theme2"
                onClick={() => handler('theme2')}
            >
            </div>
            <div
                className="header__themes__item theme3"
                onClick={() => handler('theme3')}
            >
            </div>
        </div>
    </div>
);

Header.propTypes = propTypes;
export default Header;
