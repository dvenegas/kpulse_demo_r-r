/**
 * Header styles
 */
export default (scopeSelector, theme) => {
    const {
        colors,
        myApp: { header },
    } = theme;

    return {
        [`${scopeSelector} .header`]: {
            position: header.position,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            top: header.top,
            width: header.width,
            height: 'auto',
            boxSizing: 'border-box',
            padding: header.padding,
            borderRadius: header.borderRadius,
            margin: header.margin,
            background: header.background,
            border: header.border,
            boxShadow: header.boxShadow,
            transitionProperty: 'all',
            transitionDuration: '.4s',
        },
        [`${scopeSelector} .header__title`]: {
            display: 'flex',
            justifyContent: 'flex-start',
            alignItems: 'center',
            flex: '1 0 320px',
        },
        [`${scopeSelector} .header__title img`]: {
            maxWidth: 200,
            height: 'auto',
            marginRight: 50
        },
        [`${scopeSelector} .header__title h1`]: {
            fontWeight: 600,
            color: header.title.color,
            transitionProperty: 'all',
            transitionDuration: '.4s',
        },
        [`${scopeSelector} .header__themes`]: {
            display: 'flex',
            justifyContent: 'flex-end',
            alignItems: 'center',
            flex: '1'
        },
        [`${scopeSelector} .header__themes__item`]: {
            width: header.themes.item.width,
            height: header.themes.item.height,
            margin: '0 15px',
            boxShadow: header.themes.boxShadow,
        },
        [`${scopeSelector} .header__themes .theme1`]: {
            backgroundColor: colors.theme1
        },
        [`${scopeSelector} .header__themes .theme2`]: {
            backgroundColor: colors.theme2
        },
        [`${scopeSelector} .header__themes .theme3`]: {
            backgroundColor: colors.theme3
        },
    };
};
