/**
 * main components styles
 */
 export default (scopeSelector, theme) => {
     const {
         fontFamily,
     } = theme.myApp;

     return {
         [`${scopeSelector}`]: {
             width: '100%',
             height: 'inherit',
             display: 'flex',
             flexDirection: 'column',
             justifyContent: 'center',
             alignItems: 'center',
             fontFamily: fontFamily
         }
     };
 };
