
import React, { Component } from 'react';
import Header from '../Header';
import Card from '../Card';
import BuilderStyles from '../../helpers/styles/builderStyles.jsx';


class MainComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            themeSelect: 'theme1'
        };
        this.handler = this.handler.bind(this);
        this.scopeSelector = 'mainComponents';
    }

    handler(theme) {
        this.setState({
            themeSelect: theme
        });
    }

    render() {
        const { themeSelect } = this.state;
        return (
            <div className="mainComponents">
                <BuilderStyles
                    scopeSelector={`.${this.scopeSelector}`}
                    theme={themeSelect}
                />
                <Header handler={this.handler} />
                <Card />
            </div>
        );
    }
}

export default MainComponent;
