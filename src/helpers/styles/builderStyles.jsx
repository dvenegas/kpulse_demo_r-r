import { Style } from 'radium';
import React, { PropTypes } from 'react';
import { merge } from 'lodash';

import themeDefault from './themeDefault';
import themeSelectPink from './themePink';
import themeSelectBlue from './themeBlue';

import cardStyles from '../../components/Card/CardStyles.jsx';
import headerStyles from '../../components/Header/HeaderStyles.jsx';
import mainComponentStyles from '../../components/MainComponent/MainComponentStyles.jsx';

const propTypes = {
    scopeSelector: PropTypes.string.isRequired,
    theme: PropTypes.string,
};

const selectTheme = (theme) => {
    const select = {
        theme1: themeDefault,
        theme2: themeSelectPink,
        theme3: themeSelectBlue,
    };
    return select[theme]();
};

const BuilderStyles = ({ scopeSelector, theme }) => {
    const themeSelect = selectTheme(theme);
    const margeStyles = merge(
        mainComponentStyles(scopeSelector, themeSelect),
        headerStyles(scopeSelector, themeSelect),
        cardStyles(scopeSelector, themeSelect)
    );

    return (
        <Style
            radiumConfig={{
                userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36'
            }}
            rules={margeStyles}
        />
    );
};

BuilderStyles.propTypes = propTypes;

export default BuilderStyles;
