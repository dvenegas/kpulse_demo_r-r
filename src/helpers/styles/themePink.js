export default () => {
    const colorVariables = {
        primary: '#d4377e',
        secondary: '#ffffff',
        lightPrimary: '#ec96bd',
        darkSecondary: '#afafaf',
        primary35: 'rgba(212, 55, 126, 0.35)',
        theme1: '#dadada',
        theme2: '#d4377e',
        theme3: '#44b3cc'
    };

    return {
        colors: colorVariables,
        myApp: {
            fontFamily: '"Roboto Condensed", sans-serif',
            header: {
                position: 'absolute',
                top: 0,
                width: '100%',
                padding: 15,
                borderRadius: '0 3px 0 3px',
                margin: '0px',
                border: 'none',
                background: colorVariables.lightPrimary,
                boxShadow: '0px 2px 5px 2px rgba(0, 0, 0, 0.1)',
                title: {
                    color: colorVariables.secondary,
                },
                themes: {
                    item: {
                        width: 30,
                        height: 30
                    },
                    boxShadow: `0px 2px 5px 2px ${colorVariables.primary35}`,
                },
            },
            card: {
                width: '70%',
                padding: 0,
                borderRadius: '5px',
                margin: '0px',
                border: 'none',
                background: colorVariables.primary35,
                boxShadow: '0px 2px 5px 2px rgba(0, 0, 0, 0.1)',
                header: {
                    width: '100%',
                    padding: 15,
                    borderRadius: '5px',
                    margin: '0px',
                    border: 'none',
                    color: colorVariables.secondary,
                    fontWeight: 700,
                    background: colorVariables.primary35,
                    textShadow: '1px 1px 2px rgba(150, 150, 150, 1)',
                    boxShadow: '0px 2px 5px 2px rgba(0, 0, 0, 0.1)'
                },
                info: {
                    width: '100%',
                    padding: 35,
                    color: colorVariables.darkSecondary,
                    textShadow: '1px 1px 2px rgba(150, 150, 150, 1)',
                    img: {
                        width: 50,
                        height: 50,
                        margin: '0 10px 0 0',
                        border: 'none',
                        borderRadius: '5px'
                    }
                }
            }
        }
    };
};
