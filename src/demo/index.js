/**
 * Entrypoint script demo
 */
/* global document */
/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import { render } from 'react-dom';

import RootApp from './root.jsx';

const MyApp = () =>
    <RootApp />;

render(
    <MyApp />,
    document.getElementById('root')
);
