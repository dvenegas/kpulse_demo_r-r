/**
 * Root app
 */
 /* global document */

import React from 'react';
import ReactDOMServer from 'react-dom/server';
import ResetStyles from '../components/ResetStyles';
import MainComponent from '../components/MainComponent';
import ImplementationStyles from '../components/ImplementationStyles.jsx';

const RootApp = () => {
    const styles = ReactDOMServer.renderToStaticMarkup(<ResetStyles />);
    document.head.insertAdjacentHTML('beforeEnd', styles);
    return (
        <div className="app-Module">
            <ImplementationStyles scopeSelector=".app-Module" />
            <MainComponent />
        </div>
    );
};

export default RootApp;
