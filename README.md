# KPulse Demo react and radium
---
- **Dependencies**
- **How to run ?**
- **RoadMap**
- **Docs**
---

## Dependencies

** Node.js(v0.12.4) & NPM(v2.10.1)  **

* `gcc` and `g++` 4.8 or newer, or
* `clang` and `clang++` 3.4 or newer
* Python 2.6 or 2.7
* GNU Make 3.81 or newer
* libexecinfo (FreeBSD and OpenBSD only)

More info:  [Github/node](https://github.com/nodejs/node).

** Install Node.js & NPM **
```sh
$ ./configure
$ make
$ [sudo] make install
```
## How to run ?

- Development Mode (static server + hot reload + test runner)

    ```
        npm install
        npm start
    ```
